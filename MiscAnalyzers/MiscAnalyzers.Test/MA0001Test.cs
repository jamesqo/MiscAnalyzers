﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using TestHelper;
using MiscAnalyzers;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using static TestHelper.Strings;

namespace MiscAnalyzers.Test
{
    public class MA0001Test : CodeFixVerifier
    {
        protected override CodeFixProvider GetCSharpCodeFixProvider() => new MA0001CodeFix();

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer() => new MA0001Analyzer();

        [Theory]
        [MemberData(nameof(NoDiagnostics_Data))]
        public void NoDiagnostics(string source)
        {
            VerifyCSharpDiagnostic(source, Array.Empty<DiagnosticResult>());
        }

        public static IEnumerable<object[]> NoDiagnostics_Data()
            => new[]
            {
                new object[] { string.Empty },
                // nonliteral/unnamed
                new object[]
                {
                    @"
class C
{
    void M(object o)
    {
        M(new object());
    }
}"
                },
                new object[]
                {
                    // nonliteral/unnamed, literal/named
                    @"
class C
{
    void M(object o1, object o2)
    {
        M(new object(), o2: null);
    }
}"
                }
            };

        [Theory]
        [MemberData(nameof(Diagnostics_Data))]
        public void Diagnostics(TestCase testCase)
        {
            var diagnostic = new DiagnosticResult
            {
                Id = "MA0001",
                Message = "Use named parameters when passing boolean, numeric, or null literals",
                Severity = DiagnosticSeverity.Warning,
                Locations = testCase.Locations.ToArray()
            };

            VerifyCSharpDiagnostic(testCase.OldSource, diagnostic);
            VerifyCSharpFix(testCase.OldSource, testCase.NewSource);
        }

        public static IEnumerable<object[]> Diagnostics_Data()
            => new object[][]
            {
                // numeric, integer
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(int foo)
    {
        M(0);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(int foo)
    {
        M(foo: 0);
    }
}"),
                // numeric, float
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(float f)
    {
        M(0.0f);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(float f)
    {
        M(f: 0.0f);
    }
}"),
                // numeric, double
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(double d)
    {
        M(0.0d);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(double d)
    {
        M(d: 0.0d);
    }
}"),
                // numeric, decimal
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(decimal m)
    {
        M(0.0m);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(decimal m)
    {
        M(m: 0.0m);
    }
}"),
                // true
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(bool b)
    {
        M(true);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(bool b)
    {
        M(b: true);
    }
}"),
                // false
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(bool b)
    {
        M(false);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(bool b)
    {
        M(b: false);
    }
}"),
                // null
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(object o)
    {
        M(null);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(object o)
    {
        M(o: null);
    }
}"),
                // literal/unnamed, literal/unnamed
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(int foo, int bar)
    {
        M(0, 0);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(int foo, int bar)
    {
        M(foo: 0, bar: 0);
    }
}"),
                // literal/unnamed, literal/named
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(object o1, object o2)
    {
        M(null, o2: null);
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(object o1, object o2)
    {
        M(o1: null, o2: null);
    }
}"),
                // literal/unnamed, nonliteral/unnamed
                // To avoid CS1738, we need to make all arguments after the offending argument named, even
                // if they aren't literals.
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(object o1, object o2)
    {
        M(null, new object());
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(object o1, object o2)
    {
        M(o1: null, o2: new object());
    }
}"),
                // nonliteral/unnamed, literal/unnamed, nonliteral/unnamed
                // Although we should make all subsequent arguments named, we shouldn't touch preceding ones.
                new TestCase()
                .WithOldSource(@"
class C
{
    void M(object o1, object o2, object o3)
    {
        M(new object(), null, new object());
    }
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 6, 9))
                .WithNewSource(@"
class C
{
    void M(object o1, object o2, object o3)
    {
        M(new object(), o2: null, o3: new object());
    }
}"),
            };
    }
}