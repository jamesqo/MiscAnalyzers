﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using TestHelper;
using Xunit;
using static TestHelper.Strings;

namespace MiscAnalyzers.Test
{
    public class MA0003Test : CodeFixVerifier
    {
        protected override CodeFixProvider GetCSharpCodeFixProvider() => new MA0003CodeFix();

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer() => new MA0003Analyzer();

        [Theory]
        [MemberData(nameof(NoDiagnostics_Data))]
        public void NoDiagnostics(string source)
        {
            VerifyCSharpDiagnostic(source, Array.Empty<DiagnosticResult>());
        }

        public static IEnumerable<object[]> NoDiagnostics_Data()
            => new[]
            {
                new object[] { string.Empty },
                // Single and-expression, in parentheses
                new object[]
                {
                    @"
class C
{
    bool M() => true || (true && true);
}"
                },
                // Chained and-expression, in parentheses
                new object[]
                {
                    @"
class C
{
    bool M() => true || (true && true && true);
}"
                },
                // Multiple chained and-expressions, in parentheses
                new object[]
                {
                    @"
class C
{
    bool M() => (true && true && true) || true || (true && true && true);
}"
                }
            };

        [Theory]
        [MemberData(nameof(Diagnostics_Data))]
        public void Diagnostics(TestCase testCase)
        {
            var diagnostic = new DiagnosticResult
            {
                Id = "MA0003",
                Message = "&& cannot be combined directly with ||",
                Severity = DiagnosticSeverity.Warning,
                Locations = testCase.Locations.ToArray()
            };

            VerifyCSharpDiagnostic(testCase.OldSource, diagnostic);
            VerifyCSharpFix(testCase.OldSource, testCase.NewSource);
        }

        public static IEnumerable<object[]> Diagnostics_Data()
            => new object[][]
            {
                // Single and-expression, left side
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true && true || true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 17))
                .WithNewSource(@"
class C
{
    bool M() => (true && true) || true;
}"),
                // Single and-expression, right side
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true || true && true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 25))
                .WithNewSource(@"
class C
{
    bool M() => true || (true && true);
}"),
                // Chained and-expression, left side
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true && true && true || true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 17))
                .WithNewSource(@"
class C
{
    bool M() => (true && true && true) || true;
}"),
                // Chained and-expression, right side
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true || true && true && true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 25))
                .WithNewSource(@"
class C
{
    bool M() => true || (true && true && true);
}"),
                // Multiple chained and-expressions
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true && true && true || true || true && true && true;
}")
                .WithLocations(
                    new DiagnosticResultLocation(Test0, 4, 17),
                    new DiagnosticResultLocation(Test0, 4, 45))
                .WithNewSource(@"
class C
{
    bool M() => (true && true && true) || true || (true && true && true);
}"),
                // Nested single and-expressions
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M() => true || true && (true || true && (true || true && true));
}")
                .WithLocations(
                    new DiagnosticResultLocation(Test0, 4, 25),
                    new DiagnosticResultLocation(Test0, 4, 42),
                    new DiagnosticResultLocation(Test0, 4, 59))
                .WithNewSource(@"
class C
{
    bool M() => true || (true && (true || (true && (true || (true && true)))));
}")
            };

        // TODO: Diagnostics reported by different runs of the analyzer need different DiagnosticResults,
        // even if the ID of each diagnostic is the same. Fix this so the tests can run.
    }
}
