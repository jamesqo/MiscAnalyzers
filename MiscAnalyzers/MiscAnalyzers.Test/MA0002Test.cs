﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.Diagnostics;
using TestHelper;
using Xunit;
using static TestHelper.Strings;

namespace MiscAnalyzers.Test
{
    public class MA0002Test : CodeFixVerifier
    {
        protected override CodeFixProvider GetCSharpCodeFixProvider() => new MA0002CodeFix();

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer() => new MA0002Analyzer();

        [Theory]
        [MemberData(nameof(NoDiagnostics_Data))]
        public void NoDiagnostics(string source)
        {
            VerifyCSharpDiagnostic(source, Array.Empty<DiagnosticResult>());
        }

        public static IEnumerable<object[]> NoDiagnostics_Data()
            => new[]
            {
                new object[] { string.Empty },
                // condition/nonliteral, left/nonliteral, right/nonliteral
                new object[]
                {
                    @"
class C
{
    bool M(bool b1, bool b2) => b1 ? b1 : b2;
}"
                },
                new object[]
                {
                    // condition/literal, left/nonliteral, right/nonliteral
                    @"
class C
{
    bool M(bool b1, bool b2) => true ? b1 : b2;
}"
                }
            };

        [Theory]
        [MemberData(nameof(Diagnostics_Data))]
        public void Diagnostics(TestCase testCase)
        {
            var diagnostic = new DiagnosticResult
            {
                Id = "MA0002",
                Message = "Do not use boolean literal with ternary operator",
                Severity = DiagnosticSeverity.Warning,
                Locations = testCase.Locations.ToArray()
            };

            VerifyCSharpDiagnostic(testCase.OldSource, diagnostic);
            VerifyCSharpFix(testCase.OldSource, testCase.NewSource);
        }

        // Pattern 1: expr1 ? true : expr2 -> expr1 || expr2
        // Pattern 2: expr1 ? false : expr2 -> !expr1 && expr2
        // Pattern 3: expr1 ? expr2 : true -> !expr1 || expr2
        // Pattern 4: expr1 ? expr2 : false -> expr1 && expr2
        public static IEnumerable<object[]> Diagnostics_Data()
            => new object[][]
            {
                // Pattern 1
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 ? true : b2;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 || b2;
}"),
                // Pattern 2
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 ? false : b2;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => !b1 && b2;
}"),
                // Pattern 3
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 ? b2 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => !b1 || b2;
}"),
                // Pattern 4
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 ? b2 : false;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 && b2;
}"),
                // Pattern 2: Negation of negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => !b1 ? false : b2;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 && b2;
}"),
                // Pattern 3: Negation of negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => !b1 ? b2 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => b1 || b2;
}"),
                // Pattern 1: Maintaining precedence
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 ? true : b2 && b3;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                // && has higher precedence than ||, no parentheses are needed.
                // TODO: May want to add them anyway for readability's sake.
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 || b2 && b3;
}"),
                // Pattern 2: Maintaining precedence
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 ? false : b2 || b3;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => !b1 && (b2 || b3);
}"),
                // Pattern 3: Maintaining precedence
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 ? b2 && b3 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                // TODO: Consider whether parentheses should be expected here.
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => !b1 || b2 && b3;
}"),
                // Pattern 4: Maintaining precedence
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 ? b2 || b3 : false;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 && (b2 || b3);
}"),
                // Pattern 2: Negation of parenthesized negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => (!b1) ? false : b2;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => (b1) && b2;
}"),
                // Pattern 3: Negation of parenthesized negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => (!b1) ? b2 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => (b1) || b2;
}"),
                // Pattern 2: Negation of multiple-parenthesized negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => (((((!b1))))) ? false : b2;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => (((((b1))))) && b2;
}"),
                // Pattern 3: Negation of multiple-parenthesized negation
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2) => (((((!b1))))) ? b2 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 33))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2) => (((((b1))))) || b2;
}"),
                // Pattern 2: Negation of or-expression
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 || b2 ? false : b3;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => !b1 && !b2 && b3;
}"),
                // Pattern 3: Negation of and-expression
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => b1 && b2 ? b3 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => !b1 || !b2 || b3;
}"),
                // Pattern 2: If expr1 is parenthesized, negation should maintain parentheses
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => (b1 || b2) ? false : b3;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => (!b1 && !b2) && b3;
}"),
                // Pattern 3: If expr1 is parenthesized, negation should maintain parentheses
                new TestCase()
                .WithOldSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => (b1 && b2) ? b3 : true;
}")
                .WithLocations(new DiagnosticResultLocation(Test0, 4, 42))
                .WithNewSource(@"
class C
{
    bool M(bool b1, bool b2, bool b3) => (!b1 || !b2) || b3;
}"),
            };
    }
}
