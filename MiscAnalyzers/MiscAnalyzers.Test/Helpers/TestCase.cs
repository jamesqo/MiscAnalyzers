﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHelper
{
    public struct TestCase
    {
        public static implicit operator object[](TestCase testCase) => new object[] { testCase };

        public string OldSource { get; private set; }

        public IEnumerable<DiagnosticResultLocation> Locations { get; private set; }

        public string NewSource { get; private set; }

        public TestCase WithOldSource(string oldSource)
        {
            var copy = this;
            copy.OldSource = oldSource;
            return copy;
        }

        public TestCase WithLocations(params DiagnosticResultLocation[] locations)
        {
            var copy = this;
            copy.Locations = locations;
            return copy;
        }

        public TestCase WithNewSource(string newSource)
        {
            var copy = this;
            copy.NewSource = newSource;
            return copy;
        }
    }
}
