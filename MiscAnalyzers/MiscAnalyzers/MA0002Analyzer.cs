﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using MiscAnalyzers.Internal;

namespace MiscAnalyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class MA0002Analyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "MA0002";

        private const string Title = "Do not use boolean literal with ternary operator";
        private const string MessageFormat = Title;
        private const string Description = "The && or || operators can replace usage of true/false in a ternary operator.";
        private const string Category = "Readability";

        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(HandleConditionalExpression, SyntaxKind.ConditionalExpression);
        }

        private static void HandleConditionalExpression(SyntaxNodeAnalysisContext ctx)
        {
            var ternary = (ConditionalExpressionSyntax)ctx.Node;

            if (IsBoolLiteral(ternary.WhenTrue) || IsBoolLiteral(ternary.WhenFalse))
            {
                ctx.ReportDiagnostic(Diagnostic.Create(Rule, ternary.GetLocation()));
            }
        }

        private static bool IsBoolLiteral(ExpressionSyntax expr) => ValueOfBoolLiteral(expr) != null;

        internal static bool? ValueOfBoolLiteral(ExpressionSyntax expr)
        {
            switch (expr.Kind())
            {
                case SyntaxKind.TrueLiteralExpression:
                    return true;
                case SyntaxKind.FalseLiteralExpression:
                    return false;
            }

            if (expr is ParenthesizedExpressionSyntax parenExpr)
            {
                return ValueOfBoolLiteral(parenExpr.Expression);
            }

            return null;
        }
    }
}
