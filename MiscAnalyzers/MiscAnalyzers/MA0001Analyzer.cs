using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using MiscAnalyzers.Internal;

namespace MiscAnalyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class MA0001Analyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "MA0001";

        private const string Title = "Use named parameters when passing boolean, numeric, or null literals";
        private const string MessageFormat = Title;
        private const string Description = "Named parameters should be used when passing boolean, numeric, or null literals to a method.";
        private const string Category = "Readability";

        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(HandleInvocationExpression, SyntaxKind.InvocationExpression);
        }

        private static void HandleInvocationExpression(SyntaxNodeAnalysisContext ctx)
        {
            var invocation = (InvocationExpressionSyntax)ctx.Node;

            if (TryFindUnnamedLiteralArgument(invocation, out _))
            {
                ctx.ReportDiagnostic(Diagnostic.Create(Rule, invocation.GetLocation()));
            }
        }

        internal static bool TryFindUnnamedLiteralArgument(InvocationExpressionSyntax invocation, out ArgumentSyntax argument)
        {
            var arguments = invocation.ArgumentList.Arguments;

            foreach (var arg in arguments)
            {
                if (arg.IsNamed())
                {
                    // Named parameters are already being used at this argument, so for valid C# code
                    // they are being used at all subsequent arguments too.
                    break;
                }

                if (!IsLiteral(arg))
                {
                    continue;
                }

                argument = arg;
                return true;
            }

            argument = null;
            return false;
        }

        internal static bool IsLiteral(ArgumentSyntax arg)
        {
            var expr = arg.Expression;
            if (!expr.IsKind(SyntaxKind.TrueLiteralExpression) &&
                !expr.IsKind(SyntaxKind.FalseLiteralExpression) &&
                !expr.IsKind(SyntaxKind.NullLiteralExpression) &&
                !expr.IsKind(SyntaxKind.NumericLiteralExpression))
            {
                return false;
            }

            return true;
        }
    }
}
