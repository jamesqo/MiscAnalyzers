﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;
using System.Diagnostics;
using MiscAnalyzers.Internal;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace MiscAnalyzers
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MA0002CodeFix)), Shared]
    public class MA0002CodeFix : CodeFixProvider
    {
        private const string TitleFormat = "Use {0} instead of ternary operator";

        public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(MA0002Analyzer.DiagnosticId);

        public sealed override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;
            var ternary = (ConditionalExpressionSyntax)root.FindNode(diagnosticSpan);

            SyntaxKind? operatorKind = DetermineLogicalOperatorKind(ternary);
            if (operatorKind == null)
            {
                return;
            }

            string operatorText = operatorKind == SyntaxKind.LogicalAndExpression ? "&&" : "||";
            string title = string.Format(TitleFormat, operatorText);

            context.RegisterCodeFix(
                CodeAction.Create(
                    title: title,
                    createChangedDocument: ct => Task.FromResult(UseLogicalOperator(context.Document, root, ternary)),
                    equivalenceKey: nameof(MA0002CodeFix)),
                diagnostic);
        }

        private static SyntaxKind? DetermineLogicalOperatorKind(ConditionalExpressionSyntax ternary)
        {
            bool? literalValue =
                MA0002Analyzer.ValueOfBoolLiteral(ternary.WhenTrue) ??
                MA0002Analyzer.ValueOfBoolLiteral(ternary.WhenFalse);

            switch (literalValue)
            {
                case true:
                    return SyntaxKind.LogicalOrExpression;
                case false:
                    return SyntaxKind.LogicalAndExpression;
            }

            return null;
        }

        private static Document UseLogicalOperator(Document document, SyntaxNode root, ConditionalExpressionSyntax ternary)
        {
            // expr1 ? true : expr2 -> expr1 || expr2
            // expr1 ? false : expr2 -> !expr1 && expr2
            // expr1 ? expr2 : true -> !expr1 || expr2
            // expr1 ? expr2 : false -> expr1 && expr2

            Document UseBinaryOperator(SyntaxKind operatorKind, ExpressionSyntax left, ExpressionSyntax right)
            {
                var logicalExpr = BinaryExpression(operatorKind, left, right);
                return document.WithSyntaxRoot(root.ReplaceNode(ternary, logicalExpr));
            }

            bool? literalValue = MA0002Analyzer.ValueOfBoolLiteral(ternary.WhenTrue);
            if (literalValue != null)
            {
                // We may need to parenthesize expr1 or expr2 in certain cases to maintain semantics.
                // e.g. b1 ? false : b2 || b3 equals !b1 && (b2 || b3) but not !b1 && b2 || b3.
                // Parentheses will not be added if unnecessary, however.
                ExpressionSyntax left = ternary.Condition.Parenthesize();
                ExpressionSyntax right = ternary.WhenFalse.Parenthesize();

                return literalValue == true ?
                    UseBinaryOperator(SyntaxKind.LogicalOrExpression, left, right) :
                    UseBinaryOperator(SyntaxKind.LogicalAndExpression, left.Negate(), right);
            }
            
            literalValue = MA0002Analyzer.ValueOfBoolLiteral(ternary.WhenFalse);
            if (literalValue != null)
            {
                // In "b1 ? b2 : true;", calling ToFullString() on b2's node will give "b2 " bc the space to its right is part of its trivia.
                // Therefore, we must remove trailing trivia before using b2 in the new expression, or we'll get "!b1 || b2 ;".
                // This is not an issue for the node on the false branch of the conditional.
                ExpressionSyntax left = ternary.Condition.Parenthesize();
                ExpressionSyntax right = ternary.WhenTrue.WithoutTrailingTrivia().Parenthesize();

                return literalValue == true ?
                    UseBinaryOperator(SyntaxKind.LogicalOrExpression, left.Negate(), right) :
                    UseBinaryOperator(SyntaxKind.LogicalAndExpression, left, right);
            }

            return document;
        }
    }
}
