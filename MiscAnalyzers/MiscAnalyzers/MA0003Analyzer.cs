﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using MiscAnalyzers.Internal;

namespace MiscAnalyzers
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class MA0003Analyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "MA0003";

        private const string Title = "&& cannot be combined directly with ||";
        private const string MessageFormat = Title;
        private const string Description = "In C#, && has a higher precedence than ||. Parenthesize the result of multiple &&s inside of an expression using ||.";
        private const string Category = "Readability";

        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(Rule);

        public override void Initialize(AnalysisContext context)
        {
            context.RegisterSyntaxNodeAction(HandleLogicalOr, SyntaxKind.LogicalOrExpression);
        }

        private static void HandleLogicalOr(SyntaxNodeAnalysisContext ctx)
        {
            // We care about and-expressions that are direct children of this or-expression.
            // We need not worry about those that are children of nested or-expressions. Those
            // will be handled in a different invocation of this method.

            void ReportIfLogicalAnd(ExpressionSyntax expr)
            {
                if (expr.IsKind(SyntaxKind.LogicalAndExpression))
                {
                    ctx.ReportDiagnostic(Diagnostic.Create(Rule, expr.GetLocation()));
                }
            }

            var orExpr = (BinaryExpressionSyntax)ctx.Node;
            Debug.Assert(orExpr.IsKind(SyntaxKind.LogicalOrExpression));

            ReportIfLogicalAnd(orExpr.Left);
            ReportIfLogicalAnd(orExpr.Right);
        }
    }
}
