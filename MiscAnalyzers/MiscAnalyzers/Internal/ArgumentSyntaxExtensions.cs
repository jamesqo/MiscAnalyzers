﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace MiscAnalyzers.Internal
{
    internal static class ArgumentSyntaxExtensions
    {
        public static bool IsNamed(this ArgumentSyntax argument) => argument.NameColon != null;
    }
}
