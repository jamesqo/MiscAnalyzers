﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Simplification;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace MiscAnalyzers.Internal
{
    internal static class ExpressionSyntaxExtensions
    {
        public static ExpressionSyntax Negate(this ExpressionSyntax expr)
        {
            switch (expr.Kind())
            {
                case SyntaxKind.LogicalAndExpression:
                case SyntaxKind.LogicalOrExpression:
                    // !(expr1 && expr2) equals !expr1 || !expr2
                    // !(expr1 || expr2) equals !expr1 && !expr2

                    var binaryExpr = (BinaryExpressionSyntax)expr;
                    var oppositeKind = expr.Kind() == SyntaxKind.LogicalAndExpression ?
                        SyntaxKind.LogicalOrExpression :
                        SyntaxKind.LogicalAndExpression;

                    return BinaryExpression(
                        kind: oppositeKind,
                        left: binaryExpr.Left.Negate(),
                        right: binaryExpr.Right.Negate());

                case SyntaxKind.LogicalNotExpression:
                    var pue = (PrefixUnaryExpressionSyntax)expr;
                    return pue.Operand;

                case SyntaxKind.ParenthesizedExpression:
                    // Descend into parentheses. We want (!cond) to negate to (cond), not !(!cond).
                    var parenExpr = (ParenthesizedExpressionSyntax)expr;
                    return parenExpr.WithExpression(parenExpr.Expression.Negate());
            }

            return PrefixUnaryExpression(SyntaxKind.LogicalNotExpression, expr.Parenthesize());
        }

        public static ExpressionSyntax Parenthesize(this ExpressionSyntax expr)
        {
            // Copied from http://stackoverflow.com/a/43325150/4077294
            switch (expr.RawKind)
            {
                case (int)SyntaxKind.ParenthesizedExpression:
                case (int)SyntaxKind.IdentifierName:
                case (int)SyntaxKind.QualifiedName:
                case (int)SyntaxKind.SimpleMemberAccessExpression:
                case (int)SyntaxKind.InterpolatedStringExpression:
                case (int)SyntaxKind.NumericLiteralExpression:
                case (int)SyntaxKind.StringLiteralExpression:
                case (int)SyntaxKind.CharacterLiteralExpression:
                case (int)SyntaxKind.TrueLiteralExpression:
                case (int)SyntaxKind.FalseLiteralExpression:
                case (int)SyntaxKind.NullLiteralExpression:
                    return expr;

                default:
                    return ParenthesizedExpression(expr)
                        .WithAdditionalAnnotations(Simplifier.Annotation);
            }
        }
    }
}
