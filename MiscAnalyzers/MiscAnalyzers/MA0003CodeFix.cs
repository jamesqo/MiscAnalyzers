﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;
using System.Diagnostics;
using MiscAnalyzers.Internal;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace MiscAnalyzers
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MA0003CodeFix)), Shared]
    public class MA0003CodeFix : CodeFixProvider
    {
        private const string TitleFormat = "Add parentheses";

        public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(MA0003Analyzer.DiagnosticId);

        public sealed override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;
            var expr = (ExpressionSyntax)root.FindNode(diagnosticSpan);

            context.RegisterCodeFix(
                CodeAction.Create(
                    title: TitleFormat,
                    createChangedDocument: ct => Task.FromResult(Parenthesize(context.Document, root, expr)),
                    equivalenceKey: nameof(MA0003CodeFix)),
                diagnostic);
        }

        private static Document Parenthesize(Document document, SyntaxNode root, ExpressionSyntax expr)
        {
            var newRoot = root.ReplaceNode(expr, ParenthesizedExpression(expr));
            return document.WithSyntaxRoot(newRoot);
        }
    }
}
