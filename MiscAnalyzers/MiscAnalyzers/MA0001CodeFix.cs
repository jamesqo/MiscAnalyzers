﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Rename;
using Microsoft.CodeAnalysis.Text;
using System.Diagnostics;
using MiscAnalyzers.Internal;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace MiscAnalyzers
{
    [ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MA0001CodeFix)), Shared]
    public class MA0001CodeFix : CodeFixProvider
    {
        private const string TitleFormat = "Use named parameters";

        public sealed override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(MA0001Analyzer.DiagnosticId);

        public sealed override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

        public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context)
        {
            var root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

            var diagnostic = context.Diagnostics.First();
            var diagnosticSpan = diagnostic.Location.SourceSpan;
            var invocation = (InvocationExpressionSyntax)root.FindNode(diagnosticSpan);

            context.RegisterCodeFix(
                CodeAction.Create(
                    title: TitleFormat,
                    createChangedDocument: ct => UseNamedParametersAsync(context.Document, invocation, ct),
                    equivalenceKey: nameof(MA0001CodeFix)),
                diagnostic);
        }

        private static async Task<Document> UseNamedParametersAsync(Document document, InvocationExpressionSyntax invocation, CancellationToken ct)
        {
            if (!MA0001Analyzer.TryFindUnnamedLiteralArgument(invocation, out var argument))
            {
                return document;
            }

            var arguments = invocation.ArgumentList.Arguments;
            int argIndex = arguments.IndexOf(argument);
            Debug.Assert(argIndex != -1);

            var semanticModel = await document.GetSemanticModelAsync(ct);

            // Note: To avoid CS1738, we rewrite all subsequent parameters to be named regardless of
            // whether they are literals or not.
            var newInvocation = invocation.ReplaceNodes(
                nodes: arguments.Skip(argIndex),
                computeReplacementNode: (arg, _) => MakeNamedArgument(arg, invocation, semanticModel));

            var syntaxRoot = await document.GetSyntaxRootAsync(ct);
            return document.WithSyntaxRoot(syntaxRoot.ReplaceNode(invocation, newInvocation));
        }

        private static ArgumentSyntax MakeNamedArgument(ArgumentSyntax arg, InvocationExpressionSyntax invocation, SemanticModel model)
        {
            if (arg.IsNamed())
            {
                return arg;
            }

            string paramName = FindParameterName(invocation, arg, model);
            return arg.WithNameColon(NameColon(paramName));
        }

        private static string FindParameterName(InvocationExpressionSyntax invocation, ArgumentSyntax arg, SemanticModel model)
        {
            // There's no need to use the Symbols API to find the parameter name if it's already there in the syntax.
            // Plus, named parameters don't have to be specified in order, so they could break our assumption that the
            // parameter's index in caller == the parameter's index in method definition.
            Debug.Assert(!arg.IsNamed());

            var methodSymbol = (IMethodSymbol)model.GetSymbolInfo(invocation.Expression).Symbol;
            int argIndex = invocation.ArgumentList.Arguments.IndexOf(arg);
            return methodSymbol.Parameters[argIndex].Name;
        }
    }
}
